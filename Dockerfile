FROM openresty/openresty:stretch
LABEL org.label-schema.schema-version="1.0.0"
LABEL org.label-schema.vendor="Sitepilot"
LABEL org.label-schema.name="nginx"

# Copy nginx configuration files
RUN mkdir -p /var/log/nginx; \
    mkdir -p /usr/local/openresty/nginx/conf/conf.d
COPY conf /usr/local/openresty/nginx/conf

# Create template folder
RUN mkdir -p /opt/sitepilot
ADD ./templates /opt/sitepilot/templates

ENV SP_APP_NAME="undefined" \
    SP_PHP_HOST="localhost" \
    SP_PHP_PORT="9000" \
    SP_CACHE_ENABLED="true" \
    SP_CACHE_REDIS_HOST="redis-master.default.svc.cluster.local" \
    SP_CACHE_REDIS_SLAVE="redis-slave.default.svc.cluster.local" \
    SP_CACHE_REDIS_PORT="6379"

EXPOSE 80

# Setup workdir
RUN mkdir -p /var/www/htdocs
WORKDIR /var/www/htdocs

# Add entrypoint script
COPY ./docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["/usr/bin/openresty", "-g", "daemon off;"]

# Use SIGQUIT instead of default SIGTERM to cleanly drain requests
# See https://github.com/openresty/docker-openresty/blob/master/README.md#tips--pitfalls
STOPSIGNAL SIGQUIT
