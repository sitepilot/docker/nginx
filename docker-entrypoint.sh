#!/bin/sh

log() {
    echo "[Sitepilot] $1"
}

echo ""
echo ""
echo "***************************************************"
echo "* Sitepilot Nginx Container"
echo "* "
echo "* Environment:"
echo "* - SP_APP_NAME: $SP_APP_NAME"
echo "* - SP_PHP_HOST: $SP_PHP_HOST"
echo "* - SP_PHP_PORT: $SP_PHP_PORT"
echo "* - SP_CACHE_ENABLED: $SP_CACHE_ENABLED"
echo "* - SP_CACHE_REDIS_HOST: $SP_CACHE_REDIS_HOST"
echo "* - SP_CACHE_REDIS_SLAVE: $SP_CACHE_REDIS_SLAVE"
echo "* - SP_CACHE_REDIS_PORT: $SP_CACHE_REDIS_PORT"
echo "* "
echo "* Support: https://sitepilot.io"
echo "***************************************************"
echo ""
echo ""

log "Creating default directories..."
mkdir -p /var/www/log
mkdir -p /var/www/htdocs

touch /usr/local/openresty/nginx/conf/conf.d/main.conf

if [ $SP_APP_NAME != "undefined" ]; then
    log "Generating configuration based on environment..."
    cat /opt/sitepilot/templates/main.tmpl | perl -p -e 's/\$\{([^}]+)\}/defined $ENV{$1} ? $ENV{$1} : $&/eg' > /usr/local/openresty/nginx/conf/conf.d/main.conf

    log "Generating php configuration..."
    cat /opt/sitepilot/templates/config-php.tmpl | perl -p -e 's/\$\{([^}]+)\}/defined $ENV{$1} ? $ENV{$1} : $&/eg' > /var/www/config.php

    touch /usr/local/openresty/nginx/conf/conf.d/cache.conf
    touch /usr/local/openresty/nginx/conf/conf.d/cache-config.conf
    touch /usr/local/openresty/nginx/conf/conf.d/cache-upstream.conf

    if [ "$SP_CACHE_ENABLED" = true ]; then
      log "Setup cache configuration..."
      cat /opt/sitepilot/templates/cache-upstream.tmpl | perl -p -e 's/\$\{([^}]+)\}/defined $ENV{$1} ? $ENV{$1} : $&/eg' > /usr/local/openresty/nginx/conf/conf.d/cache-upstream.conf
      cat /opt/sitepilot/templates/cache-config.tmpl | perl -p -e 's/\$\{([^}]+)\}/defined $ENV{$1} ? $ENV{$1} : $&/eg' > /usr/local/openresty/nginx/conf/conf.d/cache-config.conf
      cat /opt/sitepilot/templates/cache-on.tmpl | perl -p -e 's/\$\{([^}]+)\}/defined $ENV{$1} ? $ENV{$1} : $&/eg' > /usr/local/openresty/nginx/conf/conf.d/cache.conf
    else
      log "Disable cache configuration..."
      cat /opt/sitepilot/templates/cache-off.tmpl | perl -p -e 's/\$\{([^}]+)\}/defined $ENV{$1} ? $ENV{$1} : $&/eg' > /usr/local/openresty/nginx/conf/conf.d/cache.conf
    fi
fi

log "Starting services..."
exec "$@"